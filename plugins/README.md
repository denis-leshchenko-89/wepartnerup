# PLUGINS

**This directory contains Javascript plugins and libraries that you want to run before mounting the root Vue.js application.**

More information about the usage of this directory in [the documentation](https://nuxtjs.org/guide/plugins).

---
> ###Common Folder
> > Used for base JS scripts
> 
> ###Interceptors Folder
> > Used to intercept and process requests to the backend
>  
> ### Vue Folder
> > Used for base VueJS plugins and libraries
> 
