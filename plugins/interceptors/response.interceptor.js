// eslint-disable-next-line import/no-extraneous-dependencies
import axios from 'axios'

export default function responseInterceptor() {
 axios.interceptors.response.use((response) => response.data)
}
