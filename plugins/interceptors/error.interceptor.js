// eslint-disable-next-line import/no-extraneous-dependencies
import axios from 'axios'

export default function errorInterceptor() {
  axios.interceptors.response.use(
    (response) => response,
    (error) => Promise.reject(error),
  )
}
