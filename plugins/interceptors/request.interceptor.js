// eslint-disable-next-line import/no-extraneous-dependencies
import axios from 'axios'
import { AuthService } from '@services'

export default function requestInterceptor () {
  axios.interceptors.request.use((request) => {
    const token = AuthService.getToken()
    if (token) {
      request.headers.Authorization = token
    }
    return request
  })
}
