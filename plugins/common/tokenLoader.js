import { authActions } from '@shared/store/actions'

export default ({ store }) => {
  store.dispatch(authActions.autoLogin)
}
