# SERVICES

**This directory contains the application login, server requests and data processing.**

To create new service, make new class with service name in file
(file name should be the same as service nam but in camel сase).
Then, in the file, create instance of class, to use single ton, and
freeze instance with **Object.freeze()**. Example
---
Example:

service.js
```javascript
class Service {} 

const instance = new Service()
Object.freeze(instance)

export default instance
```

index.js
```javascript
import Service from './service'

export {
  Service  
}
```

Don't forget to export service in index.js

