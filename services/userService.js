import { userApi } from '@/shared/api'

class UserService {
  getUserProfile() {
    return userApi.userProfile()
  }

  createProfile(data) {
    return userApi.createProfile(data)
  }

  updateAccount(data) {
    return userApi.updateAccount(data)
  }

  deactivateAccount() {
    return userApi.deactivateAccount()
  }

  changePassword(data) {
    return userApi.changePassword(data)
  }
}

const instance = new UserService()
Object.freeze(instance)

export default instance
