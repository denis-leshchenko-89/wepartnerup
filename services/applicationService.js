import { applicationApi } from '@/shared/api'

class ApplicationService {
  createApplication(data) {
    return applicationApi.createApplication(data)
  }

  updateApplication(id, data) {
    return applicationApi.updateApplication(id, data)
  }

  deleteApplication(id) {
    return applicationApi.deleteApplication(id)
  }

  getApplication(id) {
    return applicationApi.getApplication(id)
  }

  fetchApplications() {
    return applicationApi.fetchApplications()
  }
}

const instance = new ApplicationService()
Object.freeze(instance)

export default instance
