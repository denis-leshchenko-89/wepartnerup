import Cookies from 'js-cookie';
import { authApi } from '@/shared/api';
import { CommonHelper } from '@/shared/helpers';
import { AUTH_TYPE } from '@/shared/constants';

class AuthService {
  async auth(data, type = AUTH_TYPE.LOGIN) {
    const user =
      type === AUTH_TYPE.REGISTRATION ? await authApi.registration(data) : await authApi.login(data);

    if (user) {
      Cookies.set('token', CommonHelper.parseToken(user.token));
    }
    return user;
  }

  forgotPassword(email) {
    return authApi.forgotPassword(email)
  }

  logout() {
    return Cookies.remove('token')
  }

  getToken() {
    return Cookies.get('token')
  }
}

const instance = new AuthService()
Object.freeze(instance)

export default instance
