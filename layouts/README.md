# LAYOUTS

**This directory contains your Application Layouts.**

Layouts are a great help when you want to change the look and feel 
of your Nuxt.js app. For example you want to include a sidebar
or have distinct layouts for mobile and desktop.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/guide/views#layouts).

