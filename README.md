# The Million Dollar Fund

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

---
While creating a new components, page, layout, store, please, follow these rules
1. Component should be readable
2. Don't use usually strings in component, better move it to the constants !!!
3. Use async/await operators instead of promises
4. Vuex actions cannot returns value, only commit to store
5. If use separate components to html, scss, index.vue file,
keep in mind, than all background images will not works as before.
To fix this, need to create global scss variable, and move picture to it.
In future, use variables with image instead of pure images in scss
7. Follow the eslint rules. In will be improved in the future and will work alone
with build app. Also will work with git hooks, to avoid dirty code
8. Use aliases which are defined in **nuxt.config.js**
9. Avoid console.log when push to repository
---
####Example of separating components (look at login in pages):
index.vue (login page)
```vue
<template src="./login.html"></template>
<script>
import Cookies from 'js-cookie'

export default {
  Normal Vue code ...
}
</script>
<style lang="scss" scoped src="./login.scss"></style>
```
login.html
```html
<div>
  Normal Html Code
</div>
```

login.scss
```scss
body {
  ...
  Normal Scss Code
}
```

