export { default as CommonConfirmModal } from '../../components/Common/ConfirmModal.vue'
export { default as CommonModal } from '../../components/Common/Modal.vue'
export { default as CommonPreloader } from '../../components/Common/Preloader.vue'
export { default as LandingFooter } from '../../components/Landing/Footer.vue'
export { default as LandingNavigationMenu } from '../../components/Landing/NavigationMenu.vue'
export { default as BaseVButton } from '../../components/base/VButton.vue'
export { default as BaseVInput } from '../../components/base/VInput.vue'
export { default as DashboardApplicationsNavigationMenu } from '../../components/Dashboard/Applications/NavigationMenu.vue'
export { default as DashboardApplicationsRoleMemberForm } from '../../components/Dashboard/Applications/RoleMemberForm.vue'
export { default as DashboardApplicationsUrlMemberForm } from '../../components/Dashboard/Applications/UrlMemberForm.vue'

export const LazyCommonConfirmModal = import('../../components/Common/ConfirmModal.vue' /* webpackChunkName: "components/common-confirm-modal" */).then(c => wrapFunctional(c.default || c))
export const LazyCommonModal = import('../../components/Common/Modal.vue' /* webpackChunkName: "components/common-modal" */).then(c => wrapFunctional(c.default || c))
export const LazyCommonPreloader = import('../../components/Common/Preloader.vue' /* webpackChunkName: "components/common-preloader" */).then(c => wrapFunctional(c.default || c))
export const LazyLandingFooter = import('../../components/Landing/Footer.vue' /* webpackChunkName: "components/landing-footer" */).then(c => wrapFunctional(c.default || c))
export const LazyLandingNavigationMenu = import('../../components/Landing/NavigationMenu.vue' /* webpackChunkName: "components/landing-navigation-menu" */).then(c => wrapFunctional(c.default || c))
export const LazyBaseVButton = import('../../components/base/VButton.vue' /* webpackChunkName: "components/base-v-button" */).then(c => wrapFunctional(c.default || c))
export const LazyBaseVInput = import('../../components/base/VInput.vue' /* webpackChunkName: "components/base-v-input" */).then(c => wrapFunctional(c.default || c))
export const LazyDashboardApplicationsNavigationMenu = import('../../components/Dashboard/Applications/NavigationMenu.vue' /* webpackChunkName: "components/dashboard-applications-navigation-menu" */).then(c => wrapFunctional(c.default || c))
export const LazyDashboardApplicationsRoleMemberForm = import('../../components/Dashboard/Applications/RoleMemberForm.vue' /* webpackChunkName: "components/dashboard-applications-role-member-form" */).then(c => wrapFunctional(c.default || c))
export const LazyDashboardApplicationsUrlMemberForm = import('../../components/Dashboard/Applications/UrlMemberForm.vue' /* webpackChunkName: "components/dashboard-applications-url-member-form" */).then(c => wrapFunctional(c.default || c))

// nuxt/nuxt.js#8607
export function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
