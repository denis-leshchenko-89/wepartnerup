# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<CommonConfirmModal>` | `<common-confirm-modal>` (components/Common/ConfirmModal.vue)
- `<CommonModal>` | `<common-modal>` (components/Common/Modal.vue)
- `<CommonPreloader>` | `<common-preloader>` (components/Common/Preloader.vue)
- `<LandingFooter>` | `<landing-footer>` (components/Landing/Footer.vue)
- `<LandingNavigationMenu>` | `<landing-navigation-menu>` (components/Landing/NavigationMenu.vue)
- `<BaseVButton>` | `<base-v-button>` (components/base/VButton.vue)
- `<BaseVInput>` | `<base-v-input>` (components/base/VInput.vue)
- `<DashboardApplicationsNavigationMenu>` | `<dashboard-applications-navigation-menu>` (components/Dashboard/Applications/NavigationMenu.vue)
- `<DashboardApplicationsRoleMemberForm>` | `<dashboard-applications-role-member-form>` (components/Dashboard/Applications/RoleMemberForm.vue)
- `<DashboardApplicationsUrlMemberForm>` | `<dashboard-applications-url-member-form>` (components/Dashboard/Applications/UrlMemberForm.vue)
