import Vue from 'vue'
import { wrapFunctional } from './index'

const components = {
  CommonConfirmModal: () => import('../../components/Common/ConfirmModal.vue' /* webpackChunkName: "components/common-confirm-modal" */).then(c => wrapFunctional(c.default || c)),
  CommonModal: () => import('../../components/Common/Modal.vue' /* webpackChunkName: "components/common-modal" */).then(c => wrapFunctional(c.default || c)),
  CommonPreloader: () => import('../../components/Common/Preloader.vue' /* webpackChunkName: "components/common-preloader" */).then(c => wrapFunctional(c.default || c)),
  LandingFooter: () => import('../../components/Landing/Footer.vue' /* webpackChunkName: "components/landing-footer" */).then(c => wrapFunctional(c.default || c)),
  LandingNavigationMenu: () => import('../../components/Landing/NavigationMenu.vue' /* webpackChunkName: "components/landing-navigation-menu" */).then(c => wrapFunctional(c.default || c)),
  BaseVButton: () => import('../../components/base/VButton.vue' /* webpackChunkName: "components/base-v-button" */).then(c => wrapFunctional(c.default || c)),
  BaseVInput: () => import('../../components/base/VInput.vue' /* webpackChunkName: "components/base-v-input" */).then(c => wrapFunctional(c.default || c)),
  DashboardApplicationsNavigationMenu: () => import('../../components/Dashboard/Applications/NavigationMenu.vue' /* webpackChunkName: "components/dashboard-applications-navigation-menu" */).then(c => wrapFunctional(c.default || c)),
  DashboardApplicationsRoleMemberForm: () => import('../../components/Dashboard/Applications/RoleMemberForm.vue' /* webpackChunkName: "components/dashboard-applications-role-member-form" */).then(c => wrapFunctional(c.default || c)),
  DashboardApplicationsUrlMemberForm: () => import('../../components/Dashboard/Applications/UrlMemberForm.vue' /* webpackChunkName: "components/dashboard-applications-url-member-form" */).then(c => wrapFunctional(c.default || c))
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}
