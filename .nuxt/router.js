import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _fd93367a = () => interopDefault(import('../pages/about.vue' /* webpackChunkName: "pages/about" */))
const _36193fc0 = () => interopDefault(import('../pages/applications/index.vue' /* webpackChunkName: "pages/applications/index" */))
const _121fd27d = () => interopDefault(import('../pages/coming-soon.vue' /* webpackChunkName: "pages/coming-soon" */))
const _2ecd29bb = () => interopDefault(import('../pages/forgot-password.vue' /* webpackChunkName: "pages/forgot-password" */))
const _3f2b6301 = () => interopDefault(import('../pages/howitworksforentrepreneurspage.vue' /* webpackChunkName: "pages/howitworksforentrepreneurspage" */))
const _4fdd155a = () => interopDefault(import('../pages/howitworksforinvestors.vue' /* webpackChunkName: "pages/howitworksforinvestors" */))
const _6a95e29f = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _50cd9406 = () => interopDefault(import('../pages/messages.vue' /* webpackChunkName: "pages/messages" */))
const _17d48d5f = () => interopDefault(import('../pages/profile.vue' /* webpackChunkName: "pages/profile" */))
const _55133798 = () => interopDefault(import('../pages/projects.vue' /* webpackChunkName: "pages/projects" */))
const _e865c81a = () => interopDefault(import('../pages/registration.vue' /* webpackChunkName: "pages/registration" */))
const _ad804fc0 = () => interopDefault(import('../pages/settings/index.vue' /* webpackChunkName: "pages/settings/index" */))
const _5772b0a4 = () => interopDefault(import('../pages/applications/calculator.vue' /* webpackChunkName: "pages/applications/calculator" */))
const _3da622c4 = () => interopDefault(import('../pages/applications/create.vue' /* webpackChunkName: "pages/applications/create" */))
const _54b47d4c = () => interopDefault(import('../pages/settings/account-information.vue' /* webpackChunkName: "pages/settings/account-information" */))
const _534e148c = () => interopDefault(import('../pages/settings/change-your-password.vue' /* webpackChunkName: "pages/settings/change-your-password" */))
const _1c161714 = () => interopDefault(import('../pages/settings/deactivate-account.vue' /* webpackChunkName: "pages/settings/deactivate-account" */))
const _600afea6 = () => interopDefault(import('../pages/Dashboard/Entrepreneur/Applications/CreateApplication/CreateApplicationPage.vue' /* webpackChunkName: "pages/Dashboard/Entrepreneur/Applications/CreateApplication/CreateApplicationPage" */))
const _10040995 = () => interopDefault(import('../pages/Dashboard/Entrepreneur/Applications/CreateApplication/FirstStepPage.vue' /* webpackChunkName: "pages/Dashboard/Entrepreneur/Applications/CreateApplication/FirstStepPage" */))
const _3da20f35 = () => interopDefault(import('../pages/Dashboard/Entrepreneur/Applications/CreateApplication/SecondStepPage.vue' /* webpackChunkName: "pages/Dashboard/Entrepreneur/Applications/CreateApplication/SecondStepPage" */))
const _b6113c68 = () => interopDefault(import('../pages/Dashboard/Entrepreneur/Applications/CreateApplication/ThirdStepPage.vue' /* webpackChunkName: "pages/Dashboard/Entrepreneur/Applications/CreateApplication/ThirdStepPage" */))
const _a7f9b758 = () => interopDefault(import('../pages/Dashboard/Entrepreneur/Applications/EditSubmittedApplicationPage/EditSubmittedApplicationPage.vue' /* webpackChunkName: "pages/Dashboard/Entrepreneur/Applications/EditSubmittedApplicationPage/EditSubmittedApplicationPage" */))
const _2d4116d7 = () => interopDefault(import('../pages/applications/edit/_id.vue' /* webpackChunkName: "pages/applications/edit/_id" */))
const _be2380f0 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _fd93367a,
    name: "about"
  }, {
    path: "/applications",
    component: _36193fc0,
    name: "applications"
  }, {
    path: "/coming-soon",
    component: _121fd27d,
    name: "coming-soon"
  }, {
    path: "/forgot-password",
    component: _2ecd29bb,
    name: "forgot-password"
  }, {
    path: "/howitworksforentrepreneurspage",
    component: _3f2b6301,
    name: "howitworksforentrepreneurspage"
  }, {
    path: "/howitworksforinvestors",
    component: _4fdd155a,
    name: "howitworksforinvestors"
  }, {
    path: "/login",
    component: _6a95e29f,
    name: "login"
  }, {
    path: "/messages",
    component: _50cd9406,
    name: "messages"
  }, {
    path: "/profile",
    component: _17d48d5f,
    name: "profile"
  }, {
    path: "/projects",
    component: _55133798,
    name: "projects"
  }, {
    path: "/registration",
    component: _e865c81a,
    name: "registration"
  }, {
    path: "/settings",
    component: _ad804fc0,
    name: "settings"
  }, {
    path: "/applications/calculator",
    component: _5772b0a4,
    name: "applications-calculator"
  }, {
    path: "/applications/create",
    component: _3da622c4,
    name: "applications-create"
  }, {
    path: "/settings/account-information",
    component: _54b47d4c,
    name: "settings-account-information"
  }, {
    path: "/settings/change-your-password",
    component: _534e148c,
    name: "settings-change-your-password"
  }, {
    path: "/settings/deactivate-account",
    component: _1c161714,
    name: "settings-deactivate-account"
  }, {
    path: "/Dashboard/Entrepreneur/Applications/CreateApplication/CreateApplicationPage",
    component: _600afea6,
    name: "Dashboard-Entrepreneur-Applications-CreateApplication-CreateApplicationPage"
  }, {
    path: "/Dashboard/Entrepreneur/Applications/CreateApplication/FirstStepPage",
    component: _10040995,
    name: "Dashboard-Entrepreneur-Applications-CreateApplication-FirstStepPage"
  }, {
    path: "/Dashboard/Entrepreneur/Applications/CreateApplication/SecondStepPage",
    component: _3da20f35,
    name: "Dashboard-Entrepreneur-Applications-CreateApplication-SecondStepPage"
  }, {
    path: "/Dashboard/Entrepreneur/Applications/CreateApplication/ThirdStepPage",
    component: _b6113c68,
    name: "Dashboard-Entrepreneur-Applications-CreateApplication-ThirdStepPage"
  }, {
    path: "/Dashboard/Entrepreneur/Applications/EditSubmittedApplicationPage/EditSubmittedApplicationPage",
    component: _a7f9b758,
    name: "Dashboard-Entrepreneur-Applications-EditSubmittedApplicationPage-EditSubmittedApplicationPage"
  }, {
    path: "/applications/edit/:id?",
    component: _2d4116d7,
    name: "applications-edit-id"
  }, {
    path: "/",
    component: _be2380f0,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
