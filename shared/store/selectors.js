/**
 * Global $store object
 * @param store
 * @returns {{isLoggedIn}}
 */
export const authSelectors = (store) => ({
  isLoggedIn: store.getters['auth/isLoggedIn'],
  authStatus: store.getters['/auth/authStatus'],
  getUserInfo: store.getters['/auth/getUserInfo'],
})

export const userSelectors = (store) => ({
  profileStatus: store.getters['user/profileStatus'],
  getUser: store.getters['user/getUser'],
})

export const applicationSelectors = (store) => ({
  applicationsStatus: store.getters['application/applicationsStatus'],
  projectPrice: store.getters['application/projectPrice'],
  getApplication: store.getters['application/getApplication'],
  getAllApplications: store.getters['application/getAllApplications'],
})
