export const mainActions = {}

export const authActions = {
  auth: 'auth/auth',
  autoLogin: 'auth/autoLogin',
  forgotPassword: 'auth/forgotPassword',
  logout: 'auth/logout',
  getUserInfo: 'auth/getUserInfo',
}

export const userActions = {
  createProfile: 'user/createProfile',
  getProfile: 'user/getProfile', // TODO I think, rename to the getUserInfo
  updateAccount: 'user/updateAccount',
  deactivateAccount: 'user/deactivateAccount',
  changePassword: 'user/changePassword',
}

export const applicationActions = {
  createApplications: 'applications/createApplications',
  updateApplication: 'applications/updateApplication',
  getApplication: 'applications/getApplication',
  deleteApplication: 'applications/deleteApplication',
  fetchApplications: 'applications/fetchApplications',
}
