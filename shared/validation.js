export default {
  email: 'Email is invalid',
  required: (field) => `${field} is required`
}
