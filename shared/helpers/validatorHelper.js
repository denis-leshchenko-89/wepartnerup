class ValidatorHelper {
  static invalid($v) {
    return $v.$dirty && $v.$invalid;
  }

  static anyError($v) {
    return $v.$anyError
  }
}

export default ValidatorHelper
