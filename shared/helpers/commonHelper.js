class CommonHelper {
  /**
   * Token object must has following data:
   * access_token: string
   * token_type: string
   * @param token
   */
  static parseToken(token) {
    if (typeof token === 'string'
      || (!Object.keys(token).includes('access_token')
        && !Object.keys(token).includes('token_type'))
    ) {
      throw new Error('Cannot parse token. Format is undefined');
    }
    const { access_token, token_type } = token;
    return `${token_type} ${access_token}`;
  }
}

export default CommonHelper;
