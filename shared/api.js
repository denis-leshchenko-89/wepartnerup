import axios from 'axios'

export const authApi = {
  login: (data) => axios.post('/api/login', data),
  registration: (data) => axios.post('/api/register', data),
  forgotPassword: (data) => axios.post('/api/forgot-password', data),
}

export const userApi = {
  userProfile: () => axios.get('/api/profile'),
  createProfile: (data) => axios.post('/api/profile/update', data),
  updateAccount: (data) => axios.post('/api/profile/account/update', data),
  deactivateAccount: () => axios.post('/api/profile/delete'),
  changePassword: (data) => axios.post('/api/profile/password/update', data),
}

export const applicationApi = {
  createApplication: (data) => axios.post('/api/application/create', data),
  updateApplication: (id, data) => axios.post(`/api/application/update/${id}`, data),
  deleteApplication: (id) => axios.post(`/api/application/delete/${id}`),
  getApplication: (id) => axios.get(`/api/application/create/${id}`),
  fetchApplications: () => axios.get('/api/application/create'),
}
