import { applicationTypes } from '@shared/store/types';
import { ApplicationService } from '@services';

export const state = () => ({
  status: '',
  projectPrice: null,
  applicationList: [],
  application: {}
});
export const mutations = {
  [applicationTypes.APPLICATION_REQUEST](state) {
    state.status = 'loading';
  },
  [applicationTypes.APPLICATION_SUCCESS](state, application) {
    state.status = 'success';
    state.application = application;
  },
  [applicationTypes.APPLICATION_LIST_SUCCESS](state, applicationList) {
    state.status = 'success';
    state.applicationList = applicationList;
  },
  [applicationTypes.APPLICATION_ERROR](state) {
    state.status = 'error';
  },
  [applicationTypes.PROJECT_PRICE](state, price) {
    state.projectPrice = price;
  }
};
export const actions = {
  async createApplication({ commit }, formData) {
    await commit(applicationTypes.APPLICATION_REQUEST);
    const app = await ApplicationService.createApplication(formData);
    // TODO For whole app need to make universal function to add application
    if (app) {
      await commit(applicationTypes.APPLICATION_SUCCESS, app);
    }
  },
  async updateApplication({ commit }, {
    formData,
    id
  }) {
    await commit(applicationTypes.APPLICATION_REQUEST);
      const app = await ApplicationService.updateApplication(id, formData)
      if (app) {
        await commit(applicationTypes.APPLICATION_SUCCESS, app)
      }
    },
    async deleteApplication({ commit }, id) {
      await commit(applicationTypes.APPLICATION_REQUEST)
      await ApplicationService.deleteApplication(id)
      // TODO What todo after delete ??
    },
    async getApplication({ commit }, id) {
      await commit(applicationTypes.APPLICATION_REQUEST)
      const app = await ApplicationService.getApplication(id)
      if (app) {
        await commit(applicationTypes.APPLICATION_SUCCESS, app);
      }
    },
  async fetchApplications({ commit }) {
    await commit(applicationTypes.APPLICATION_REQUEST);
    const appList = await ApplicationService.fetchApplications();
    if (appList) {
      await commit(applicationTypes.APPLICATION_LIST_SUCCESS, appList);
    }
  }
}
export const getters = {
  applicationsStatus: (state) => state.status,
  projectPrice: (state) => state.projectPrice,
  getApplication: (state) => state.application,
  getAllApplications: (state) => state.applicationList
};

