import { userTypes } from '@shared/store/types'
import { UserService } from '@services'

export const state = () => ({
  status: '',
  user: {},
})

export const mutations = {
  [userTypes.PROFILE_REQUEST](state) {
    state.status = 'loading'
  },
  [userTypes.PROFILE_SUCCESS](state, user) {
    state.status = 'success'
    state.user = user
  },
  [userTypes.PROFILE_ERROR](state) {
    state.status = 'error'
  },
}
export const actions = {
  async createProfile({ commit }, profile) {
    await commit(userTypes.PROFILE_REQUEST)
    const user = await UserService.createProfile(profile)
    if (user) {
      await commit(userTypes.PROFILE_SUCCESS, user)
    }
  },
  async getProfile({ commit }) {
    await commit(userTypes.PROFILE_REQUEST)
    const user = await UserService.getUserProfile()
    if (user) {
      await commit(userTypes.PROFILE_SUCCESS, user)
    }
  },
  async updateAccount({ commit }, data) {
    await commit(userTypes.PROFILE_REQUEST)
    const user = await UserService.updateAccount(data)
    await commit(userTypes.PROFILE_SUCCESS, user)
  },
  async deactivateAccount({ commit }) {
    await commit(userTypes.PROFILE_REQUEST)
    const user = await UserService.deactivateAccount()
    if (user) {
      // TODO Build universal function to check if user exists and save it
      await commit(userTypes.PROFILE_SUCCESS, user)
    }
  },
  async changePassword({ commit }, data) {
    await commit(userTypes.PROFILE_REQUEST)
    const user = await UserService.changePassword(data)
    if (user) {
      await commit(userTypes.PROFILE_SUCCESS, user)
    }
  },
}
export const getters = {
  profileStatus: (state) => state.status,
  getUser: (state) => state.user,
}
