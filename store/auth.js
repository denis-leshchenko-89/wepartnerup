import { AuthService, UserService } from '@/services'

import { authTypes } from '@shared/store/types'

export const state = () => ({
  status: '',
  token: AuthService.getToken(),
  user: {},
})

export const mutations = {
  [authTypes.AUTH_REQUEST](state) {
    state.status = 'loading'
  },
  [authTypes.AUTH_SUCCESS](state, { user, token }) {
    state.status = 'success'
    state.token = token
    state.user = user
  },
  [authTypes.AUTH_ERROR](state) {
    state.status = 'error'
  },
  [authTypes.SAVE_TOKEN](state, token) {
    state.token = token
  },
  [authTypes.LOGOUT](state) {
    state.status = ''
    state.token = ''
    state.user = ''
  },
}
export const actions = {
  async auth({ commit, state }, { data, type }) {
    await commit(authTypes.AUTH_REQUEST)

    const response = await AuthService.auth(data, type)
    if (response) {
      await commit(authTypes.AUTH_SUCCESS, {
        token: response.token,
        user: response,
      })
      await this.$router.push('/profile')
    }
  },
  async autoLogin({ commit }) {
    const token = AuthService.getToken()
    if (token) {
      await commit(authTypes.SAVE_TOKEN, token)
    }
  },
  async forgotPassword({ commit }, email) {
    await commit(authTypes.AUTH_REQUEST)
    await AuthService.forgotPassword(email)
  },

  async logout({ commit }) {
    await AuthService.logout()
    await commit(authTypes.LOGOUT)
  },

  /**
   * // TODO This function need to move to the own user store.
   * // Also, when do you use it?
   * @param commit
   * @returns {Promise<void>}
   */
  async getUserProfile({ commit }) {
    await commit(authTypes.AUTH_REQUEST)
    const response = await UserService.getUserProfile()
    if (response) {
      await commit(authTypes.AUTH_SUCCESS, {
        token: response.token, // TODO No Need to store token while getting user profile
        user: response,
      })
    }
  },
}
export const getters = {
  isLoggedIn: (state) => !!state.token,
  authStatus: (state) => state.status,
  getUserInfo: (state) => state.user,
}
