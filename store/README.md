# STORE

**This directory contains your Vuex Store files. Vuex Store option is implemented in the Nuxt.js framework.**

Creating a file in this directory automatically activates the option in the framework.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/guide/vuex-store).

---
To create new module in service, just make a new file in this folder with module name **(keep in mind that this name is used while dispatch
action)**

Don't forget, the state, in module, must be a function and return state object.

Like this:

```javascript
export const state = () => ({
  someField: {},
})
```

Not this:

```javascript
export const state = {
  someField: {},
}
```

To use mutations, actions and getters, define it in global place
(@shared/store/**.js)

- mutations - in **types.js**
- actions - in **actions.js**
- getters - in **selectors.js**

